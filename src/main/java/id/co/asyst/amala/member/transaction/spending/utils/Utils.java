package id.co.asyst.amala.member.transaction.spending.utils;

import com.google.gson.Gson;
import org.apache.camel.Exchange;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {
	

	/*public String getProperty(Exchange exchange) {
		return (String)exchange.getProperty(LogAssistant.LOG_ORIGINAL_REQ);
	}*/

    public void validateRequest(Exchange exchange) {
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);

        String[] properties = new String[]{"memberid", "awardmiles", "tiermiles", "frequency", "isfee", "feeforupdate"};
        String message = validate(requestData, properties);
        if (message != null) {
            exchange.setProperty("resmsg", message);
            return;
        }
        if ((isEmpty(requestData.get("certificateid")) && isEmpty(requestData.get("customtrxcode"))) ||
                (!isEmpty(requestData.get("certificateid")) && !isEmpty(requestData.get("customtrxcode")))) {
            exchange.setProperty("resmsg", "Transaction not valid");
        }

    }
    public void validateRequestFixer(Exchange exchange) {
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);

        String[] properties = new String[]{"memberid", "awardmiles", "tiermiles", "frequency", "isfee", "feeforupdate","trxid","createddate"};
        String message = validate(requestData, properties);
        if (message != null) {
            exchange.setProperty("resmsg", message);
            return;
        }
        if ((isEmpty(requestData.get("certificateid")) && isEmpty(requestData.get("customtrxcode"))) ||
                (!isEmpty(requestData.get("certificateid")) && !isEmpty(requestData.get("customtrxcode")))) {
            exchange.setProperty("resmsg", "Transaction not valid");
        }

    }

    public void convertRequestDataValue(Exchange exchange) {
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);

        int awardMiles = (int) Double.parseDouble(requestData.get("awardmiles").toString());
        int tierMiles = (int) Double.parseDouble(requestData.get("tiermiles").toString());
        int frequency = (int) Double.parseDouble(requestData.get("frequency").toString());

        awardMiles = awardMiles < 0 ? awardMiles * -1 : awardMiles;
        tierMiles = tierMiles < 0 ? tierMiles * -1 : tierMiles;
        frequency = frequency < 0 ? frequency * -1 : frequency;

        requestData.put("awardmiles", awardMiles);
        requestData.put("tiermiles", tierMiles);
        requestData.put("frequency", frequency);

        exchange.setProperty("requestdata", requestData);
    }

    public void validateTransaction(Exchange exchange) {
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);
        Map<String, Object> customTransaction = exchange.getProperty("customtransaction", Map.class);
        List<Map<String, Object>> transaction = exchange.getProperty("transaction", List.class);

        if (!isEmpty(requestData.get("customtrxcode")) && customTransaction == null) {
            exchange.setProperty("rescode", "9003");
            exchange.setProperty("resdesc", "Data Not Found");
            exchange.setProperty("resmsg", "Custom Transaction(" + requestData.get("customtrxcode") + ") Not Found");
            return;
        }

        if (!isEmpty(requestData.get("certificateid"))) {
            if (requestData.get("isfee").equals(true)) {
                if (transaction == null || transaction.size() == 0) {
                    exchange.setProperty("rescode", "9003");
                    exchange.setProperty("resdesc", "Data not Found");
                    exchange.setProperty("resmsg", "Can't find transaction that you want to charge");
                }
                for (int i = 0; i < transaction.size(); i++) {
                    if (transaction != null && transaction.get(i).get("isfee").equals(true) && transaction.get(i).get("feeforupdate").equals(false)) {
                        exchange.setProperty("rescode", "9005");
                        exchange.setProperty("resdesc", "Invalid Data");
                        exchange.setProperty("resmsg", "Transaction already cancelled");
                    }
                }
            } else {
                for (int i = 0; i < transaction.size(); i++) {
                    if (transaction != null && transaction.get(i).get("isfee").equals(false)) {
                        exchange.setProperty("rescode", "9005");
                        exchange.setProperty("resdesc", "Invalid Data");
                        exchange.setProperty("resmsg", "Transaction is Duplicate");
                    }
                }
            }
        }


    }
    public void validateTransactionFixer(Exchange exchange) {
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);
        Map<String, Object> customTransaction = exchange.getProperty("customtransaction", Map.class);
        List<Map<String, Object>> transaction = exchange.getProperty("transaction", List.class);

        if (!isEmpty(requestData.get("customtrxcode")) && customTransaction == null) {
            exchange.setProperty("rescode", "9003");
            exchange.setProperty("resdesc", "Data Not Found");
            exchange.setProperty("resmsg", "Custom Transaction(" + requestData.get("customtrxcode") + ") Not Found");
            return;
        }

        if (!isEmpty(requestData.get("certificateid"))) {
            if (requestData.get("isfee").equals(true)) {
                if (transaction == null || transaction.size() == 0) {
                    exchange.setProperty("rescode", "9003");
                    exchange.setProperty("resdesc", "Data not Found");
                    exchange.setProperty("resmsg", "Can't find transaction that you want to charge");
                }
                for (int i = 0; i < transaction.size(); i++) {
                    if (transaction != null && transaction.get(i).get("isfee").equals(true) && transaction.get(i).get("feeforupdate").equals(false)) {
                        exchange.setProperty("rescode", "9005");
                        exchange.setProperty("resdesc", "Invalid Data");
                        exchange.setProperty("resmsg", "Transaction already cancelled");
                    }
                }
            } else {
                for (int i = 0; i < transaction.size(); i++) {
                    if (transaction != null && transaction.get(i).get("isfee").equals(false)) {
                        exchange.setProperty("rescode", "9005");
                        exchange.setProperty("resdesc", "Invalid Data");
                        exchange.setProperty("resmsg", "Transaction is Duplicate");
                    }
                }
            }
        }
        int awardmilestrx=0, tiermilestrx=0, frequencytrx=0;
        awardmilestrx = (int) Double.parseDouble(requestData.get("awardmiles").toString()) * -1;
        tiermilestrx = (int) Double.parseDouble(requestData.get("tiermiles").toString()) * -1;
        frequencytrx = (int) Double.parseDouble(requestData.get("frequency").toString()) * -1;

        exchange.setProperty("awardmilestrx",awardmilestrx);
        exchange.setProperty("tiermilestrx",tiermilestrx);
        exchange.setProperty("frequencytrx",frequencytrx);
    }

    public void checkMiles(Exchange exchange) {
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);
        List<Map<String, Object>> memberAccount = exchange.getProperty("originalmemberaccount", List.class);

        int awardMiles = (int) Double.parseDouble(memberAccount.get(0).get("awardmiles").toString());
        int tierMiles = (int) Double.parseDouble(memberAccount.get(0).get("tiermiles").toString());
        int frequency = (int) Double.parseDouble(memberAccount.get(0).get("frequency").toString());

        boolean invalid = Double.parseDouble(requestData.get("awardmiles").toString()) > awardMiles;
        invalid = Double.parseDouble(requestData.get("tiermiles").toString()) > tierMiles || invalid;
        invalid = Double.parseDouble(requestData.get("frequency").toString()) > frequency || invalid;

        exchange.setProperty("tiermilesbefore", tierMiles);
        exchange.setProperty("awardmilesbefore",awardMiles);
        exchange.setProperty("frequencybefore",frequency);

        exchange.getOut().setBody(invalid ? 1 : 0);
    }

    public void milesAfter (Exchange exchange){
        List<Map<String, Object>> memberAccount = exchange.getProperty("memberAccOri", List.class);

        int awardMiles = (int) Double.parseDouble(memberAccount.get(0).get("awardmiles").toString());
        int tierMiles = (int) Double.parseDouble(memberAccount.get(0).get("tiermiles").toString());
        int frequency = (int) Double.parseDouble(memberAccount.get(0).get("frequency").toString());

        exchange.setProperty("tiermilesafter",tierMiles);
        exchange.setProperty("awardmilesafter",awardMiles);
        exchange.setProperty("frequencyafter",frequency);
    }
    public void milesAccountDetail(Exchange exchange){
        List<Map<String, Object>> memberAccountDetail = exchange.getProperty("originalaccountdetail", List.class);
        int awardMiles;
        int tierMiles;
        int frequency;
        for (int i=0;i<memberAccountDetail.size();i++){
            awardMiles = (int) Double.parseDouble(memberAccountDetail.get(i).get("awardmiles").toString());
            tierMiles = (int) Double.parseDouble(memberAccountDetail.get(i).get("tiermiles").toString());
            frequency = (int) Double.parseDouble(memberAccountDetail.get(i).get("frequency").toString());
            memberAccountDetail.get(i).put("tiermiles",tierMiles);
            memberAccountDetail.get(i).put("awardmiles",awardMiles);
            memberAccountDetail.get(i).put("frequency",frequency);
        }
        exchange.setProperty("originalaccountdetail",memberAccountDetail);

    }

    public void convertAccountDataValue(Exchange exchange) {
        Map<String, Object> memberAccount = exchange.getProperty("memberaccount", Map.class);
        List<Map<String, Object>> accountDetails = (List<Map<String, Object>>) memberAccount.get("accountdetails");

        for (Map<String, Object> accountDetail : accountDetails) {
            accountDetail.put("awardmiles", (int) Double.parseDouble(accountDetail.get("awardmiles").toString()));
            accountDetail.put("tiermiles", (int) Double.parseDouble(accountDetail.get("tiermiles").toString()));
            accountDetail.put("frequency", (int) Double.parseDouble(accountDetail.get("frequency").toString()));
        }

        memberAccount.put("accountdetails", accountDetails);
        exchange.setProperty("memberaccount", memberAccount);
    }

    public void setResponseSuccess(Exchange exchange) {
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);
        Map<String, Object> transaction = exchange.getProperty("transaction", Map.class);
        Map<String, Object> customTransaction = exchange.getProperty("customtransaction", Map.class);
        String milesisnol = exchange.getProperty("milesisnol",String.class);
        Map<String, Object> response = new HashMap<>();

        response.put("trxid", transaction.get("trxid"));
        response.put("trxtype", "SPENDING");
        response.put("trxdate", requestData.get("trxdate"));
        response.put("memberid", requestData.get("memberid"));
        response.put("isfee", requestData.get("isfee"));
        response.put("awardmiles", -Integer.parseInt(requestData.get("awardmiles").toString()));
        response.put("tiermiles", -Integer.parseInt(requestData.get("tiermiles").toString()));
        response.put("frequency", -Integer.parseInt(requestData.get("frequency").toString()));
        response.put("certificateid", requestData.get("certificateid"));
        response.put("customtrxcode", requestData.get("customtrxcode"));
        response.put("feeforupdate", requestData.get("feeforupdate"));
        response.put("sourcetrxid", requestData.get("sourcetrxid"));
        response.put("notes", requestData.get("notes"));
        response.put("awardmilesbefore",removeDecimal(transaction.get("awardmilesbefore").toString()));
        response.put("tiermilesbefore",removeDecimal(transaction.get("tiermilesbefore").toString()));
        response.put("frequencybefore",removeDecimal(transaction.get("frequencybefore").toString()));
        System.out.println("milesisnol :: "+milesisnol);
        if (milesisnol != null){
            if (milesisnol.toUpperCase().equals("TRUE")){
                response.put("awardmilesafter",removeDecimal(transaction.get("awardmilesbefore").toString()));
                response.put("tiermilesafter",removeDecimal(transaction.get("tiermilesbefore").toString()));
                response.put("frequencyafter",removeDecimal(transaction.get("frequencybefore").toString()));
            }
        } else {
            response.put("awardmilesafter",removeDecimal(transaction.get("awardmilesafter").toString()));
            response.put("tiermilesafter",removeDecimal(transaction.get("tiermilesafter").toString()));
            response.put("frequencyafter",removeDecimal(transaction.get("frequencyafter").toString()));
        }

        if (customTransaction != null)
            response.put("customtrxname", customTransaction.get("customtrxname"));

        exchange.setProperty("responsedata", response);
    }

    private boolean isEmpty(Object str) {
        return str == null || str.toString().equals("");
    }

    private String validate(Map<String, Object> data, String[] properties) {
        for (String property : properties)
            if (data.get(property) == null || data.get(property).toString().equals(""))
                return property + " is required";
        return null;
    }
    public static int removeDecimal(String number) {

        Double dValue = Double.parseDouble(number);
        return dValue.intValue();

    }

    public void checkMemberLock(Exchange exchange) {
        Map<String, Object> mapBody = exchange.getProperty("memberLockResult", Map.class);
        List<Map<String, Object>> resultObj = (List<Map<String, Object>>) mapBody.get("result");

        for (Map<String, Object> map:resultObj) {
            boolean active = (boolean) map.get("active");
            if (active) {
                boolean blockRedeem = (boolean) map.get("blockredeem");
                if (blockRedeem) {
                    exchange.setProperty("lockFlag", true);
                }
            }
        }
    }
    public void setTrxid(Exchange exchange){
        Map<String, Object> requestData = exchange.getProperty("requestdata", Map.class);
        String trxid = requestData.get("trxid").toString();
        Map<String, Object> transaction = new HashMap<>();
        transaction.put("trxid",trxid);
        exchange.setProperty("transaction",transaction);
    }
}
